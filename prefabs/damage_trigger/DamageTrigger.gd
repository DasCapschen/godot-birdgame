tool
extends Area

"""
A Trigger that will deal damage to the anyone who enters and supports taking damage.
Use negative damage to heal.
You can make subobjects editable to change the collision shape. Do NOT edit the timer.
TODO: extend with different damage types?
"""

export var damage_per_tick: = 10.0 setget set_damage_per_tick
export var tick_time: = 1.0
export var one_shot: = false
export var delay_first_damage: = false

onready var tick_timer: Timer = $TickTimer

var damagee: Node = null

func set_damage_per_tick(val : float) -> void:
	damage_per_tick = val
	if Engine.is_editor_hint():
		if val < 0:
			$EditorIcon.texture = load("res://gui/icons/health.svg") as Texture
		else:
			$EditorIcon.texture = load("res://gui/icons/damage.svg") as Texture

func _ready() -> void:
	if not Engine.is_editor_hint():
		$EditorIcon.queue_free()

	tick_timer.wait_time = tick_time
	tick_timer.one_shot = one_shot
	tick_timer.autostart = false

func _on_tick_damage():
	if damagee != null:
		damagee._on_take_damage(damage_per_tick)
	pass

func _on_body_entered(body: Node) -> void:
	#QUESTION: is has_method() fast enough?
	#or would checking is_in_group("damage") be faster?
	if body.has_method("_on_take_damage"):
		damagee = body
	else:
		return
	
	if not delay_first_damage:
		_on_tick_damage()
	tick_timer.start()


func _on_body_exited(body: Node) -> void:
	if body == damagee:
		damagee = null
		tick_timer.stop()
