extends Spatial
class_name SpawnPoint, "res://gui/icons/spawn.svg"

# if false, cannot spawn here
export var is_active: = true

#0-100, higher number will be picked first, if possible
export var priority: = 0

func get_spawn_position() -> Vector3:
	$RayCast.force_raycast_update()
	return $RayCast.get_collision_point()

func _ready() -> void:
	#remove editor debug items
	$EditorIcon.queue_free()
	$debug_arrow.queue_free()

	#make sure it is in the SpawnPoint group
	if not is_in_group("SpawnPoint"):
		add_to_group("SpawnPoint", true)
