tool
extends Area
class_name CheckPoint, "res://gui/icons/checkpoint.png"

"""
Checkpoint for saving progress throughout a level.
Can also be a spawnpoint at the same time. (added as child node)
SpawnPoint will be disabled until checkpoint is activated
"""

signal checkpoint_activated(checkpoint)

export var is_active: = true
var has_been_activated: = false

var spawn_point: Node = null
var spawn_point_prefab: = preload("res://prefabs/spawn_point/SpawnPoint.tscn")
export var is_also_spawnpoint: = false setget set_is_also_spawnpoint
func set_is_also_spawnpoint(value: bool) -> void:
	if Engine.is_editor_hint():
		is_also_spawnpoint = value
		if value:
			spawn_point = spawn_point_prefab.instance()
			add_child(spawn_point)
			spawn_point.set_owner(get_tree().edited_scene_root)
			#$SpawnPoint/EditorIcon.hide()
		elif spawn_point:
			spawn_point.queue_free()
	pass

func _ready() -> void:
	if not Engine.is_editor_hint():
		$EditorIcon.queue_free()
	
	#make sure is in CheckPoints groups
	if not is_in_group("CheckPoint"):
		add_to_group("CheckPoint", true)
	
	if spawn_point:
		spawn_point.is_active = has_been_activated

#this trigger only "collides" with players!
#must be a player! (or a wrongly assigned node...)
func _on_body_entered(body: Node) -> void:
	if is_active and not has_been_activated:
		is_active = false
		has_been_activated = true
		if spawn_point: 
			spawn_point.is_active = true
		#GameSaver.save_game( get_tree().current_scene.get_instance_id() ) # TODO: proper id
		Events.emit_signal("checkpoint_activated", self)
	pass

func _on_body_exited(body: Node) -> void:
	pass # Replace with function body.
