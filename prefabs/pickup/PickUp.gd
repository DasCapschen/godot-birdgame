extends Spatial

onready var model: = $Model
export var rotation_rate: = deg2rad(90.0)
export var score: int = 1

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	model.rotate(Vector3.UP, rotation_rate * delta)

#collides only with players! (see collision layers)
func _on_body_entered(body: Node) -> void:
	assert(body.has_method("_on_pickup_collected"))
	body._on_pickup_collected(self)
	queue_free()
