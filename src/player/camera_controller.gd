extends Spatial

var pitch = 0.0
export var max_pitch_default = deg2rad(70.0)
export var min_pitch_default = -deg2rad(30.0)

var max_pitch = max_pitch_default
var min_pitch = min_pitch_default

const SAVE_KEY = "player_camera_pivot"


"""
TODO: if controller and state == Fly then rotate camera with left stick!
"""

func _input(event):
	if event is InputEventMouseMotion:
		if abs(event.relative.x) > Config.mouse_smooth:
			yaw(-deg2rad(event.relative.x) * Config.mouse_speed.x)
		if abs(event.relative.y) > Config.mouse_smooth:
			var amount = -deg2rad(event.relative.y) * Config.mouse_speed.y
			if Config.mouse_flip_y:
				amount = -amount
			pitch(amount)

func pitch(amount):
	pitch = clamp(pitch + amount, min_pitch, max_pitch)
	rotation.x = pitch

func yaw(amount):
	rotate(Vector3.UP, amount)

func _process(delta: float):
	var pitch = Input.get_action_strength("camera_down") - Input.get_action_strength("camera_up")
	var amount = pitch * delta * Config.mouse_speed.y
	if Config.mouse_flip_y:
		amount = -amount
	pitch(amount)
	
	var yaw = Input.get_action_strength("camera_left") - Input.get_action_strength("camera_right")
	yaw(yaw * delta * Config.mouse_speed.x)
	
	return

func _on_save_game(save_data: SaveGame) -> void:
	var data: Dictionary = {}
	data["rotation"] = rotation
	save_data.data[SAVE_KEY] = data

func _on_load_game(save_data: SaveGame) -> void:
	if not SAVE_KEY in save_data.data:
		printerr("Savegame is missing %s data!" % SAVE_KEY)
		return
	
	var data = save_data.data[SAVE_KEY]
	rotation = data["rotation"]
	pitch = -rotation.x
