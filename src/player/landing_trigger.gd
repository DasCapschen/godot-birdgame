extends Area

func _ready():
	connect("body_entered", self, "_on_body_entered")

#if we started flying, turn on the Activation Timer
#if we stopped flying, disable the Collision Shape
func _on_state_changed(state):
	if state == "fly":
		$ActivationTimer.start()
	else:
		$CollisionShape.disabled = true
	pass

#if the timer finished, enable the Collision Shape
func _on_ActivationTimer_timeout():
	$CollisionShape.disabled = false
	pass

func _on_body_entered(body : Node):
	if body.is_in_group("landable"):
		#TODO: magic
		pass
	pass
