extends KinematicBody
class_name PlayerController

"TODO: use RigidBody with Character Mode?"

#setup references to other nodes we need to access
#(or that any other node might want to access, since we are owner)
onready var fsm: = $FiniteStateMachine
onready var collider: = $CollisionShape
onready var animation_player: = $Model/AnimationPlayer
onready var animation_tree: = $AnimationTree
onready var model = $Model
onready var camera: = get_node(camera_path)
onready var double_tap_timer: = $DoubleTapTimer

#constants
const FLOOR_NORMAL: = Vector3.UP
const SAVE_KEY: = "player_controller" #unique?

#exports
export var camera_path: NodePath

export var damage_min_speed: = 5.0
export var damage_max_speed: = 10.0

export var health_default: = 100.0
export var health_max: = 100.0

export var kill_height: = -10.0

#vars
var health: = health_default
var is_alive: = true
var score: int = 0

#disable player... if we ever need this? maybe when paused...
var is_active: = true setget set_is_active
func set_is_active(val : bool) -> void:
	is_active = val
	if not collider:
		return
	collider.disabled = not is_active
	#set_process(val)
	#set_process_input(val)
	#set_process_unhandled_input(val)
	#set_physics_process(val)
	#fsm._is_active = val


#Landing Trigger is deactived
#when starting flight, timer is started
#when timer completes, landing trigger is actived
#when user gets close to landable object, land

func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	double_tap_timer.wait_time = ProjectSettings.get_setting("application/input/double_tap_time")
	Events.connect("checkpoint_activated", self, "_on_checkpoint_activated")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("quick_save"):
		GameSaver.save_game(GameSaver.QUICK_SAVE_SLOT)
	elif event.is_action_pressed("quick_load"):
		GameSaver.load_game(GameSaver.QUICK_SAVE_SLOT)
	
	if event.is_action_pressed("ui_cancel"):
		Events.emit_signal("pause_game")
	
	if event.is_action_pressed("debug_spawn"):
		fsm.change_to("Dead")
	elif event.is_action_pressed("debug_restart_level"):
		get_tree().reload_current_scene()
	pass

func _physics_process(delta: float) -> void:
	if is_alive and global_transform.origin.y < kill_height:
		fsm.change_to("Dead")

func on_collision(collision : KinematicCollision) -> void:
	var speed = collision.collider_velocity.length()
	var dmg = clamp(inverse_lerp(damage_min_speed, damage_max_speed, speed), 0, 1)
	_on_take_damage(dmg * 100.0)

#use negative damage to heal
func _on_take_damage(damage : float) -> void:
	if not is_alive: 
		return
	
	health = clamp(health - damage, 0, health_max)
	if health <= 0:
		is_alive = false
		fsm.change_to("Dead")

func _on_pickup_collected(pickup: Node) -> void:
	assert("score" in pickup)
	score += pickup.score
	pass


func _on_save_game(save_data: Resource) -> void:
	var data: Dictionary = {}
	data["current_state"] = fsm.get_path_to(fsm.state)
	data["health"] = health
	data["score"] = score
	data["global_transform"] = global_transform
	save_data.data[SAVE_KEY] = data

func _on_load_game(save_data: Resource) -> void:
	print("loading player")
	
	if not SAVE_KEY in save_data.data:
		printerr("Savegame missing %s data!" % SAVE_KEY)
		return

	var data: Dictionary = save_data.data[SAVE_KEY]
	health = data["health"]
	score = data["score"]
	global_transform = data["global_transform"]
	
	fsm.change_to(data["current_state"])
	animation_tree.playback.start( animation_tree.get_anim_for_state(data["current_state"]) )

func _on_checkpoint_activated(checkpoint):
	print("yay, I have activated a checkpoint!")
