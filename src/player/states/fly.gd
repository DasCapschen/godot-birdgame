extends FSMState

onready var _move = get_parent()

export var fly_speed: = 10.0
export var wing_strength: = 5.0

var is_about_to_flap: = false

#flap wings
func _on_wings_changed(val: bool) -> void:
	if is_about_to_flap:
		flap()
		is_about_to_flap = false
	else:
		is_about_to_flap = true
	
	owner.double_tap_timer.start()
	yield(owner.double_tap_timer, "timeout")
	is_about_to_flap = false

func flap():
	_move.velocity.y = wing_strength

# use msg to send data between states
func enter(msg: Dictionary = {}) -> void:
	_move.enter(msg)
	
	#set move vars
	_move.max_speed = fly_speed
	_move.lock_to_xz_plane = false
	
	#connect signals
	_move.connect("wings_extended_changed", self, "_on_wings_changed")
	
	#set cam vars
	owner.camera.max_pitch = deg2rad(89.0)
	owner.camera.min_pitch = -deg2rad(89.0)
	return

func exit() -> void:
	#reset cam vars
	owner.camera.max_pitch = owner.camera.max_pitch_default
	owner.camera.min_pitch = owner.camera.min_pitch_default
	
	#reset rotation and make sure we are on floor
	#TODO: I don't really like this, because it happens ALWAYS
	owner.rotation.x = 0
	owner.move_and_collide(Vector3(0, -1000, 0))
	
	#reset move vars
	_move.lock_to_xz_plane = true
	_move.max_speed = _move.max_speed_default
	
	#disconnect signals
	_move.disconnect("wings_extended_changed", self, "_on_wings_changed")
	_move.exit()
	return

func input(event: InputEvent) -> void:
	_move.input(event)
	return

func unhandled_input(event: InputEvent) -> void:
	_move.unhandled_input(event)
	return

func process(delta: float) -> void:
	_move.process(delta)
	return

func physics_process(delta: float) -> void:
	_move.physics_process(delta)
	
	if owner.is_on_floor():
		if _move.get_move_direction().length_squared() > 0:
			fsm.change_to("Move/Ground/Walk")
		fsm.change_to("Move/Ground/Idle")
	return
