extends FSMState

func _on_anim_finished(_anim: String) -> void:
	fsm.change_to("Move/Ground/Idle")

# use msg to send data between states
func enter(_msg: Dictionary = {}) -> void:
	_respawn()
	
	owner.is_active = false

	#does not work...
	#owner.animation_player.connect("animation_finished", self, "_on_anim_finished")
	
	#TODO: FIXME: HACK
	yield(get_tree().create_timer(4), "timeout")
	fsm.change_to("Move/Ground/Idle")

func exit() -> void:
	owner.is_active = true
	owner.health = owner.health_default
	owner.is_alive = true

func _respawn():
	#find spawn point
	var spawn_points: Array = get_tree().get_nodes_in_group("SpawnPoint")
	spawn_points.sort_custom(self, "_sort_spawn_points")
	var spawn_to_use = null
	for s in spawn_points:
		if s.is_active:
			spawn_to_use = s
			break
	
	#fail if no spawn point found
	if spawn_to_use == null:
		print("Game Over")
		return
	
	#move to spawn point and set forward vector
	var pos = spawn_to_use.get_spawn_position()
	var dir = spawn_to_use.global_transform.basis.z
	owner.global_transform.origin = pos
	owner.global_transform = owner.global_transform.looking_at(pos + dir, owner.FLOOR_NORMAL)

func _sort_spawn_points(a, b):
	if a.priority > b.priority:
		return true
	return false 
