extends FSMState

onready var _ground = get_parent()
onready var _move = _ground.get_parent()

# use msg to send data between states
func enter(msg: Dictionary = {}) -> void:
	_ground.enter(msg)
	#STOP moving when entering idle
	_ground.get_parent().velocity = Vector3.ZERO
	return

func exit() -> void:
	_ground.exit()
	return

func input(event: InputEvent) -> void:
	_ground.input(event)
	return

func unhandled_input(event: InputEvent) -> void:
	_ground.unhandled_input(event)
	return

func process(delta: float) -> void:
	_ground.process(delta)
	return

func physics_process(delta: float) -> void:
	#checks is_on_floor()
	_ground.physics_process(delta)
	
	if _move.get_move_direction().length_squared() > 0:
		fsm.change_to("Move/Ground/Walk")
	return
