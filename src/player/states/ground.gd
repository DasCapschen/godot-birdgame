extends FSMState

"""
Parent State for Bird Behaviour while it is moving on ground.
"""

# parent is "Move"

var fly_taps: = 0
export var jump_force = 10.0

# use msg to send data between states
func enter(msg: Dictionary = {}) -> void:
	get_parent().enter(msg)
	get_parent().connect("wings_extended_changed", self, "_on_wings_changed")
	return

#TODO: we should NOT call exit on parent if we transition to FLY !!!!
func exit() -> void:
	get_parent().disconnect("wings_extended_changed", self, "_on_wings_changed")
	get_parent().max_speed = get_parent().max_speed_default
	get_parent().exit()
	return

func input(event: InputEvent) -> void:
	get_parent().input(event)
	return

func unhandled_input(event: InputEvent) -> void:
	get_parent().unhandled_input(event)
	return

func _on_wings_changed(val: bool) -> void:
	if val:
		fly_taps += 1
		if fly_taps > 1:
			jump()
			fly_taps = 0
		else:
			owner.double_tap_timer.start()
			yield(owner.double_tap_timer, "timeout")
			fly_taps = 0
	pass

func jump():
	get_parent().velocity.y = jump_force
	pass

func process(delta: float) -> void:
	get_parent().process(delta)
	return

func physics_process(delta: float) -> void:
	get_parent().physics_process(delta)

	if not owner.is_on_floor():
		fsm.change_to("Move/Fly")
	return
