extends FSMState

func enter(_msg: Dictionary = {}) -> void:
	#deactivate player
	owner.is_active = false
	owner.is_alive = false
	
	#wait for death animation to finish
	#DOES NOT WORK
	#yield(owner.animation_player, "animation_finished")
	
	yield(get_tree().create_timer(4), "timeout")
	fsm.change_to("Spawn")
	return

func exit() -> void:
	return

func input(_event: InputEvent) -> void:
	return

func unhandled_input(_event: InputEvent) -> void:
	return

func process(_delta: float) -> void:
	return

func physics_process(_delta: float) -> void:
	return
