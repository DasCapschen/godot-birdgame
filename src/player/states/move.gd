extends FSMState

"""
Parent State for any movement of the bird, ground and air.
"""

#movement "constants"
export var max_speed_default: = 1.0
export var acceleration_default: = 8.0
export var deceleration_default: = 16.0
export var turn_rate_default:  = 5.0 * PI / 180.0
export var gravity_default: = Vector3(0, -20, 0)
export var wings_lift_default: = Vector3(0, 18, 0)

export var lock_to_xz_plane: = true

#movement variables
var max_speed: = max_speed_default
var acceleration: = acceleration_default
var deceleration: = deceleration_default
var turn_rate: = turn_rate_default
var gravity: = gravity_default
var velocity: = Vector3.ZERO
var wings_lift: = wings_lift_default


var _debug_acc: = "acceleration"
var _debug_lift: = Vector3.ZERO

signal wings_extended_changed(wings)
var are_wings_extended: = false setget set_wings_extended
func set_wings_extended(val: bool) -> void:
	are_wings_extended = val
	emit_signal("wings_extended_changed", val)


func unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("fly"):
		self.are_wings_extended = true
	elif event.is_action_released("fly"):
		self.are_wings_extended = false
	pass


func physics_process(delta: float) -> void:
	#reduce gravity depending on wings
	#do not lift if jumping!
	var lift: = calculate_lift()
	
	velocity += (gravity + lift) * delta

	var movement: Vector3 = velocity
	movement.y = 0

	var direction: Vector3 = get_move_direction()

	var acc: float
	if direction.dot(movement) > 0:
		acc = acceleration
		_debug_acc = "acceleration"
	else:
		acc = deceleration
		_debug_acc = "deceleration"
		
	movement = calculate_velocity(movement, max_speed, acc, delta, direction)
	
	#owner.look_at() is global, this would be local
	if direction.length_squared() > 0:
		var target = owner.transform.looking_at(owner.transform.origin + direction, owner.FLOOR_NORMAL)
		owner.transform = owner.transform.interpolate_with(target, 0.1)
	
	velocity.x = movement.x
	velocity.z = movement.z
	velocity = owner.move_and_slide(velocity, owner.FLOOR_NORMAL)
	return

func calculate_lift() -> Vector3:
	var lift: = Vector3.ZERO
	if velocity.y < 0 and are_wings_extended:
		lift = wings_lift
		#reduce lift depending on direction
		var dir = -owner.global_transform.basis.z
		var cosine = dir.dot(owner.FLOOR_NORMAL)
		lift *= 1.0 - pow(cosine, 2) #TODO: change the 2 to some const
	_debug_lift = lift
	return lift

func get_move_direction() -> Vector3:
	#get camera direction with Y up and Z on plane (remove x rot)
	var cam_basis: Basis = owner.camera.global_transform.basis
	
	if lock_to_xz_plane:
		cam_basis = cam_basis.rotated(cam_basis.x, -cam_basis.get_euler().x)
	
	var dir = Vector3(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"), 
		0, 
		Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	)
	return cam_basis.xform(dir).normalized()


static func calculate_velocity(
	old_velocity : Vector3,
	max_speed : float,
	acceleration : float,
	delta : float,
	direction : Vector3
) -> Vector3:
	var target = direction * max_speed
	return old_velocity.move_toward(target, acceleration * delta)
