extends FSMState

onready var _ground = get_parent()
onready var _move = _ground.get_parent()

export var run_speed: = 3.0

#signal running(true) if started running, else running(false)
signal running(start)

# use msg to send data between states
func enter(msg: Dictionary = {}) -> void:
	_ground.enter(msg)

	if Input.is_action_pressed("run"):
		_move.max_speed = run_speed
		emit_signal("running", true)
	return

func exit() -> void:
	_ground.exit()
	_move.max_speed = _move.max_speed_default
	return

func input(event: InputEvent) -> void:
	_ground.input(event)
	return

func unhandled_input(event: InputEvent) -> void:
	_ground.unhandled_input(event)

	if event.is_action_pressed("run"):
		_move.max_speed = run_speed
		emit_signal("running", true)
	elif event.is_action_released("run"):
		_move.max_speed = _move.max_speed_default
		emit_signal("running", false)
	return

func process(delta: float) -> void:
	_ground.process(delta)
	return

func physics_process(delta: float) -> void:
	#handles leaving floor
	_ground.physics_process(delta)

	if _move.velocity.length_squared() < 0.0001:
		fsm.change_to("Move/Ground/Idle")
	return
