extends Resource
class_name SaveGame

"""
Contains the data for a save game.
'Template' for savegame.
"""

export var game_version: String = ""
export var active_scene: String = ""
export var data: Dictionary = {}