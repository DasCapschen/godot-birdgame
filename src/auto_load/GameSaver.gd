extends Node

"""
Contains the logic to save and load savegames.
Singleton.
"""

const SAVE_GAME = preload("res://src/auto_load/SaveGame.gd")
const SAVE_FOLDER = "user://savegames"
const SAVE_FILENAME_TEXT = "%03d.tres"
const SAVE_FILENAME_BINARY = "%03d.res"
const QUICK_SAVE_SLOT = 0

onready var _use_binary_saves: bool = ProjectSettings.get_setting("application/config/use_binary_saves")
onready var _game_version: String = ProjectSettings.get_setting("application/config/version")

func save_game(slot: int) -> void:
	var save_data: = SAVE_GAME.new()
	save_data.game_version = _game_version

	#save current scene (freed nodes)
	save_data.active_scene = get_tree().current_scene.filename
	get_tree().current_scene._on_save_game(save_data)

	#save node data
	for node in get_tree().get_nodes_in_group("save"):
		node._on_save_game(save_data)

	var dir: = Directory.new()
	if not dir.dir_exists(SAVE_FOLDER):
		dir.make_dir_recursive(SAVE_FOLDER)
	
	var save_path: String
	var flags: int
	if _use_binary_saves:
		save_path = SAVE_FOLDER.plus_file(SAVE_FILENAME_BINARY % slot)
		flags = ResourceSaver.FLAG_COMPRESS
	else:
		save_path = SAVE_FOLDER.plus_file(SAVE_FILENAME_TEXT % slot)
		flags = 0
	
	if ResourceSaver.save(save_path, save_data, flags) != OK:
		printerr("Error: Could not save game!")
	pass

func load_game(slot: int) -> void:
	var save_path: String
	if _use_binary_saves:
		save_path = SAVE_FOLDER.plus_file(SAVE_FILENAME_BINARY % slot)
	else:
		save_path = SAVE_FOLDER.plus_file(SAVE_FILENAME_TEXT % slot)
	
	var file: = File.new()
	if not file.file_exists(save_path):
		printerr("Error: Save file does not exist!")
		return
	
	var save_data: Resource = load(save_path)

	if _game_version != save_data.game_version:
		print("Game and Save version mismatch!")

	print("changing scene")
	
	#change scene
	if get_tree().change_scene(save_data.active_scene) != OK:
		printerr("Error: Could not load scene %s" % save_data.active_scene)

	print("awaiting next frame...")

	#is this the best method??
	yield(get_tree(), "idle_frame")

	print("loading scene")
	
	#load scene
	get_tree().current_scene._on_load_game(save_data)

	print("loading nodes")

	#load objects
	for node in get_tree().get_nodes_in_group("save"):
		node._on_load_game(save_data)
	pass
