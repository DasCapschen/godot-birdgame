extends Node

"""
Declare Global Signals here.
Allows communication between objects without directly connecting them.
"""

signal checkpoint_activated(checkpoint)


signal change_scene(scene_path) #request scene change
signal scene_changed() #singal that scene change finished

signal start_game(scene) # go from main menu to game
signal stop_game() # go from game to main menu

signal pause_game() # go from game to pause menu
signal unpause_game() # go from pause menu to game

signal exit() # exit to desktop

signal player_instance_changed(instance)
