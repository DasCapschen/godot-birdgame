extends Node

#config config
const CONFIG_PATH = "user://settings.cfg"
var config_file = ConfigFile.new()

func _ready():
	if config_file.load( CONFIG_PATH ) != null :
		load_all_settings()
	pass

func load_all_settings():
	locale = config_file.get_value("general", "language", 0)
	physics = config_file.get_value("general", "physics", 1)
	
	reflections = config_file.get_value("graphics", "reflections", 1)
	shadow_res = config_file.get_value("graphics", "shadowres", 2)
	shadow_quality = config_file.get_value("graphics", "shadowquality", 1)
	msaa = config_file.get_value("graphics", "msaa", 0)
	aniso = config_file.get_value("graphics", "aniso", 4)
	
	fullscreen = config_file.get_value("display", "fullscreen", false)
	render_scale = config_file.get_value("display", "render_scale", 1.0)
	
	master_vol = config_file.get_value("volume", "master", 0)
	effects_vol = config_file.get_value("volume", "effects", 0)
	music_vol = config_file.get_value("volume", "music", -6)
	dialog_vol = config_file.get_value("volume", "dialog", 0)
	
	mouse_speed = config_file.get_value("input", "mouse_speed", Vector2(0.12, 0.12))
	mouse_smooth = config_file.get_value("input", "mouse_smooth", 0.16)
	mouse_flip_y = config_file.get_value("input", "flip_y", false)

#language
const LANGUAGES = ["en", "de"]
var locale: int = 0 setget set_language
func set_language(val):
	locale = val
	config_file.set_value("general", "language", val )
	config_file.save(CONFIG_PATH)
	
	TranslationServer.set_locale(LANGUAGES[val])


#physics
const PHYSICS_FPS = [30, 60, 120]
var physics: int = 1 setget set_physics
func set_physics(val):
	physics = val
	config_file.set_value("general", "physics", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("physics/common/physics_fps", PHYSICS_FPS[val])
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")


#reflections
const REFLECTION_QUALITY = [
	[512, false, false],
	[1024, true, false],
	[2048, true, true],
	[4096, true, true]
]
var reflections: int = 2 setget set_reflections
func set_reflections(val):
	reflections = val
	config_file.set_value("graphics", "reflections", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("rendering/quality/reflections/atlas_size", REFLECTION_QUALITY[val][0])
	ProjectSettings.set_setting("rendering/quality/reflections/texture_array_reflections", REFLECTION_QUALITY[val][1])
	ProjectSettings.set_setting("rendering/quality/reflections/high_quality_ggx", REFLECTION_QUALITY[val][2])
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")



#shadow resolution
const SHADOW_RES_OPTIONS = [512, 1024, 2048, 4096, 8192]
var shadow_res: int = 2 setget set_shadowres
func set_shadowres(val):
	shadow_res = val
	config_file.set_value("graphics", "shadowres", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("rendering/quality/directional_shadow/size", SHADOW_RES_OPTIONS[val])
	ProjectSettings.set_setting("rendering/quality/shadow_atlas/size", SHADOW_RES_OPTIONS[val])
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")

#shadow filtering
# 0 = no filter
# 1 = PCF 9
# 2 = PCF 13
var shadow_quality: int = 2 setget set_shadowquality
func set_shadowquality(val):
	shadow_quality = val
	config_file.set_value("graphics", "shadowquality", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("rendering/quality/shadows/filter_mode", val)
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")

#MSAA
# 0 = disabled
# 1 = x2
# 2 = x4
# 3 = x8
# 4 = x16
# using our own signal because we don't want to MSAA our 2D GUI, only 3D world
signal msaa_changed(val)
var msaa: int = 0 setget set_msaa
func set_msaa(val):
	msaa = val
	emit_signal("msaa_changed", val)
	config_file.set_value("graphics", "msaa", val)
	config_file.save(CONFIG_PATH)

#Anisotropic Filtering
const ANISO_OPTIONS = [1,2,4,8,16]
var aniso: int = 0 setget set_aniso
func set_aniso(val):
	aniso = val
	config_file.set_value("graphics", "aniso", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("rendering/quality/filters/anisotropic_filter_level", ANISO_OPTIONS[val])
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")


#fullscreen
var fullscreen: bool = false setget set_fullscreen
func set_fullscreen(val):
	fullscreen = val
	config_file.set_value("display", "fullscreen", val)
	config_file.save(CONFIG_PATH)
	
	ProjectSettings.set_setting("display/window/size/fullscreen", val)
	if ProjectSettings.save() != OK:
		printerr("Failed to save Project Settings!")
	
	OS.window_fullscreen = val

#render scale
signal render_scale_changed(val)
var render_scale: float = 1.0 setget set_render_scale
func set_render_scale(val):
	render_scale = val
	emit_signal("render_scale_changed", val)
	config_file.set_value("display", "render_scale", val)
	config_file.save(CONFIG_PATH)

#master volume
var master_vol: float = 0 setget set_master_vol
func set_master_vol(val :float):
	master_vol = val
	config_file.set_value("volume", "master", val)
	config_file.save(CONFIG_PATH)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), val)


#effects volume
var effects_vol: float = 0 setget set_effects_vol
func set_effects_vol(val):
	effects_vol = val
	config_file.set_value("volume", "effects", val)
	config_file.save(CONFIG_PATH)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Effects"), val)


#music volume
var music_vol: float = -6 setget set_music_vol
func set_music_vol(val):
	music_vol = val
	config_file.set_value("volume", "music", val)
	config_file.save(CONFIG_PATH)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), val)

#music volume
var dialog_vol: float = 0 setget set_dialog_vol
func set_dialog_vol(val):
	dialog_vol = val
	config_file.set_value("volume", "dialog", val)
	config_file.save(CONFIG_PATH)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Dialog"), val)

#mouse speed
var mouse_speed: Vector2 = Vector2(1.0, 1.0) setget set_mouse_speed
func set_mouse_speed(val: Vector2):
	mouse_speed = val
	config_file.set_value("input", "mouse_speed", val)
	config_file.save(CONFIG_PATH)

#mouse "smooth" (fake smooth)
var mouse_smooth: float = 0.33 setget set_mouse_smooth
func set_mouse_smooth(val: float):
	mouse_smooth = val
	config_file.set_value("input", "mouse_smooth", val)
	config_file.save(CONFIG_PATH)

var mouse_flip_y: bool = false setget set_mouse_flip_y
func set_mouse_flip_y(val: bool):
	mouse_flip_y = val
	config_file.set_value("input", "flip_y", val)
	config_file.save(CONFIG_PATH)
