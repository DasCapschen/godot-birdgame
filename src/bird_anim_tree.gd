extends AnimationTree

var playback: AnimationNodeStateMachinePlayback

var wing_blend
var wing_blend_target
const wing_blend_up_speed = 1 / 0.05
const wing_blend_down_speed = 1 / 0.15

var _active_anim

func _ready():
	yield(owner, "ready")
	anim_player = owner.animation_player.get_path()

	#get playback params of anim tree
	playback = get("parameters/StateMachine/playback")

	#get wing blend
	wing_blend = get("parameters/Wings/blend_position")
	wing_blend_target = wing_blend
	
	#make sure anim tree is actually active
	active = true
	_active_anim = playback.get_current_node()
	pass

func _process(delta):
	var up : int = (wing_blend_target > wing_blend)
	var blend_speed = wing_blend_up_speed * up + wing_blend_down_speed * (1-up)
	
	wing_blend = lerp(wing_blend, wing_blend_target, blend_speed * delta)
	set("parameters/Wings/blend_position", wing_blend)
	
	_active_anim = playback.get_current_node()
	pass

func _on_state_changed(state: String) -> void:
	if state == "Idle":
		$IdleTimer.start()
	else:
		$IdleTimer.stop()
	
	var anim = get_anim_for_state(state)
	if _active_anim == "Eat" or anim == "Spawn":
		playback.start(anim)
	else:
		playback.travel(anim)
	pass

func get_anim_for_state(state: String) -> String:
	match state:
		"Walk": return "Walk"
		"Idle": return "Idle"
		"Fly": return "Idle"
		"Spawn": return "Spawn"
		"Dead": return "Dead"
		_: return "Idle" #default
	pass

func _on_IdleTimer_timeout() -> void:
	playback.travel("Eat")

func _on_wings_changed(wings) -> void:
	wing_blend_target = int(wings)

func _on_toggle_running(start: bool) -> void:
	playback.travel("Jump" if start else "Walk")
