extends Node
class_name FSMState, "res://gui/icons/state.svg"

"""
State Interface for Finite State Machine.
"""

onready var fsm = _get_fsm(self)

#return FSM, but need to use Node, else we get cyclic error
func _get_fsm(node: Node) -> Node:
	if node != null and not node.is_in_group("FiniteStateMachine"):
		return _get_fsm(node.get_parent())
	return node

# use msg to send data between states
func enter(_msg: Dictionary = {}) -> void:
	return

func exit() -> void:
	return

func input(_event: InputEvent) -> void:
	return

func unhandled_input(_event: InputEvent) -> void:
	return

func process(_delta: float) -> void:
	return

func physics_process(_delta: float) -> void:
	return
