extends Node
class_name FiniteStateMachine, "res://gui/icons/state_machine.svg"

"""
Finite State Machine. 
"""

#initial state, select from inspector
export var initial_state: NodePath

#active state
onready var state: FSMState = get_node(initial_state) setget set_state
onready var _state_name: = state.name
signal state_changed(name)

func set_state(val : FSMState) -> void:
	state = val
	_state_name = state.name
	emit_signal("state_changed", _state_name)

var _is_active: = true setget _set_is_active
func _set_is_active(val: bool) -> void:
	set_process(val)
	set_physics_process(val)
	set_process_input(val)
	set_process_unhandled_input(val)


"""
owner = node at top of scene
player
|- model
|- fsm
   |- idle
player is the 'owner'
"""
func _ready() -> void:
	yield(owner, "ready") #wait for owner to be ready
	emit_signal("state_changed", _state_name)
	state.enter()


# change from current state to target state
func change_to(target_path : String, msg: Dictionary = {}) -> void:
	assert( has_node(target_path) )
	
	#stop if node does not exist
	if not has_node(target_path):
		return
	
	#exit current state
	state.exit()
	
	#change state and enter new state
	self.state = get_node(target_path)
	state.enter(msg)



""" CALLBACKS """
func _input(event : InputEvent) -> void:
	state.input(event)

func _unhandled_input(event : InputEvent) -> void:
	state.unhandled_input(event)

func _process(delta : float) -> void:
	state.process(delta)

func _physics_process(delta : float) -> void:
	state.physics_process(delta)
