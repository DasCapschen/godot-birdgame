extends Viewport

func _ready() -> void:
	get_viewport().connect("size_changed", self, "resize")
	Config.connect("render_scale_changed", self, "resize")
	Config.connect("msaa_changed", self, "_on_msaa_changed")
	msaa = Config.msaa

func _on_msaa_changed(val):
	msaa = val

func resize():
	size = get_viewport().size * Config.render_scale
