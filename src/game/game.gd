extends Control

onready var pause_menu: = $GUI/PauseMenu/PauseMenu
onready var main_menu: = $GUI/MainMenu/MainMenu
onready var ingame_gui: = $GUI/IngameGUI/IngameUI
onready var loading_screen: = $GUI/LoadingScreen/LoadingScreen

onready var viewport_3d: = $"ViewportContainer/3DViewport"
onready var root_3d: = $"ViewportContainer/3DViewport/3DRoot"

var player = null
var player_prefab = preload("res://src/player/player_with_camera.tscn")

var game_started: = false

func _ready() -> void:
	Events.connect("start_game", self, "_on_start_game")
	Events.connect("pause_game", self, "_on_pause_game")
	Events.connect("unpause_game", self, "_on_unpause_game")
	Events.connect("stop_game", self, "_on_stop_game")
	Events.connect("exit", self, "_on_exit")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_home"):
		var viewport: Viewport = $"ViewportContainer/3DViewport"
		viewport.size = get_viewport().size * 0.25

func _on_start_game(scene):
	main_menu.hide()
	loading_screen.show()
	
	Events.emit_signal("change_scene", scene)
	yield(Events, "scene_changed")
	
	#TESTING!!!
	#yield(get_tree().create_timer(10), "timeout")
	
	loading_screen.hide()
	pause_menu.hide()
	ingame_gui.show()
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	player = player_prefab.instance()
	root_3d.add_child(player)
	Events.emit_signal("player_instance_changed", player)
	
	get_tree().paused = false

func _on_stop_game():
	get_tree().reload_current_scene()

func _on_pause_game():
	get_tree().paused = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	pause_menu.show()

func _on_unpause_game():
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pause_menu.hide()

func _on_exit():
	get_tree().quit()
