extends Spatial

var current_scene: Node = null

func _ready() -> void:
	current_scene = $"3DMenuScene"
	#get_tree().current_scene = self.current_scene
	
	Events.connect("change_scene", self, "_on_change_scene")
	

func _on_change_scene(scene) -> void:
	call_deferred("_deferred_change_scene", scene)
	pass

func _deferred_change_scene(scene) -> void:
	#destroy current scene
	current_scene.free()
	
	#load new scene
	var new_scene = load(scene)
	current_scene = new_scene.instance()
	add_child(current_scene)
	
	yield(get_tree(), "idle_frame")
	Events.emit_signal("scene_changed")
