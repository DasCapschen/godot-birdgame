tool
extends MultiMeshInstance

#gotta rework the script, it kinda behaves weirdly...

export var width: float = 50.0
export var depth: float = 50.0

export var x_spacing: = Vector2(2, 7)
export var z_spacing: = Vector2(2, 7)
export var min_scale: = Vector2(0.5, 0.5)
export var max_scale: = Vector2(1.5, 1.5)

export(bool) var update: = false setget update_mesh
func update_mesh(_val: bool) -> void:
	var x: = 0.0
	var z: = 0.0
	var i: = 0
	
	#make new multimesh (ensure that it is unique)
	var mesh_to_use = multimesh.mesh
	multimesh = MultiMesh.new()
	multimesh.mesh = mesh_to_use
	multimesh.color_format = MultiMesh.COLOR_NONE
	multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
	multimesh.transform_format = MultiMesh.TRANSFORM_3D
	
	multimesh.instance_count = (width * depth) / min(x_spacing.x, z_spacing.x)
	
	while z < depth:
		while x < width:
			var w_scale = rand_range(min_scale.x, max_scale.x)
			var h_scale = rand_range(min_scale.y, max_scale.y)
			var scale = Vector3(w_scale, h_scale, w_scale)
			
			var z_rand = rand_range(0, z_spacing.x)
			
			var position = Vector3(x, 0, z + z_rand) * scale.inverse()
			var transform = Transform().translated(position).scaled(scale)
			
			multimesh.set_instance_transform(i, transform)
			i += 1
			
			x += rand_range(x_spacing.x, x_spacing.y)
		#end while (x)
		
		z += rand_range(z_spacing.x, z_spacing.y)
		x = rand_range(0, x_spacing.x)
	#end while (z)
	
	multimesh.visible_instance_count = i
	set_owner(get_tree().edited_scene_root)
