tool
extends Spatial

#array of node paths
export(Array, NodePath) var do_not_save: = []

var deletables: = []

func _ready() -> void:
	if Engine.is_editor_hint():
		if not is_in_group("save"):
			add_to_group("save", true)
			set_owner(get_tree().edited_scene_root)
	
	#save all nodes that might be deleted later
	for pickup in get_tree().get_nodes_in_group("PickUp"):
		deletables.append(pickup.get_path())
	pass

#write down which nodes were deleted
func _on_save_game(save_data: SaveGame) -> void:
	var data: Dictionary = {}

	for node in deletables:
		data[node] = has_node(node)

	save_data.data["active_scene"] = data
	pass

#delete nodes that didn't exist when saving
func _on_load_game(save_data: SaveGame) -> void:
	if not "active_scene" in save_data.data:
		printerr("Savegame is missing active_scene data!")
		return
	
	var data: Dictionary = save_data.data["active_scene"]
	for node in deletables:
		if data[node] == false:
			get_node(node).queue_free()
	pass
