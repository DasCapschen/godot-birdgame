 # Bird Game
A game about being a bird. You can fly around. Yay!

## Attribution
Some icons are used from Flaticon with free license. Attribution:  
Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)  
`gui/icons/spawn.svg`  

Some icons are used from Kenney's game asset packs:  
`gui/icons/damage.svg`  
`gui/icons/health.svg`  


Some icons and scripts are used from GDQuest.  
`gui/icons/state.svg`   
`gui/icons/state_machine.svg`  
`gui/debug/DebugDock.gd`  
`gui/debug/DebugPanel.gd`  
`gui/debug/DebugPanel.tscn`   


Uses cc0 textures from texturehaven.com:  
`assets/textures/forest_ground` - https://texturehaven.com/tex/?c=terrain&t=forrest_ground_01  

Uses cc0 skies from hdrihaven.com:  
`assets/skies/lakes_4k.hdr` - https://hdrihaven.com/hdri/?c=skies&h=lakes  


Uses cc0 assets from OpenGameArt.org:  
`assets/vegetation/bushes/` - https://opengameart.org/content/bushes  
`assets/vegetation/grass/` - https://opengameart.org/content/grass-pack-02  


Uses realistic water shader for godot. See `legal/Water.License`.  


Uses Mozilla's Fira Sans font: https://mozilla.github.io/Fira/  
See `legal/Fira.License`
