extends MarginContainer

var player_instance = null
func set_player_instance(instance):
	player_instance = instance

func _ready() -> void:
	Events.connect("player_instance_changed", self, "set_player_instance")
