extends Control

"""
NOTE:
	if settings don't apply, try VisualServer ?
	
	stretch 2d means we stretch the scene to fit the screen and THEN render
	stretch viewport means we render the scene and THEN scale the result!
	
	shrink is resolution scaling IF stretch is not disabled!
	eg. shrink = 4 means 25% rendering res
	
	use aspect = keep height for landscape mode
	use aspect = keep width for portrait mode
"""

signal close_menu()

func _ready():
	create_options()
	translate_labels()

func create_options():
	$Buttons/Language/Dropdown.add_item(tr("LANG_EN"))
	$Buttons/Language/Dropdown.add_item(tr("LANG_DE"))
	$Buttons/Language/Dropdown.selected = Config.config_file.get_value("general", "language", 0)
	
	$Buttons/Fullscreen/CheckBox.pressed = Config.fullscreen
	
	$Buttons/AnisoFilter/Dropdown.add_item(tr("OPTION_OFF"))
	$Buttons/AnisoFilter/Dropdown.add_item("x2")
	$Buttons/AnisoFilter/Dropdown.add_item("x4")
	$Buttons/AnisoFilter/Dropdown.add_item("x8")
	$Buttons/AnisoFilter/Dropdown.add_item("x16")
	$Buttons/AnisoFilter/Dropdown.selected = Config.aniso
	
	$Buttons/MSAA/Dropdown.add_item(tr("OPTION_OFF"))
	$Buttons/MSAA/Dropdown.add_item("x2")
	$Buttons/MSAA/Dropdown.add_item("x4")
	$Buttons/MSAA/Dropdown.add_item("x8")
	$Buttons/MSAA/Dropdown.selected = Config.msaa
	
	$Buttons/ShadowRes/Dropdown.add_item("512")
	$Buttons/ShadowRes/Dropdown.add_item("1024")
	$Buttons/ShadowRes/Dropdown.add_item("2048")
	$Buttons/ShadowRes/Dropdown.add_item("4096")
	$Buttons/ShadowRes/Dropdown.add_item("8192")
	$Buttons/ShadowRes/Dropdown.selected = Config.shadow_res
	
	$Buttons/ShadowQuality/Dropdown.add_item(tr("OPTION_LOW"))
	$Buttons/ShadowQuality/Dropdown.add_item(tr("OPTION_GOOD"))
	$Buttons/ShadowQuality/Dropdown.add_item(tr("OPTION_BETTER"))
	$Buttons/ShadowQuality/Dropdown.selected = Config.shadow_quality
	
	$Buttons/Reflections/Dropdown.add_item(tr("OPTION_OFF"))
	$Buttons/Reflections/Dropdown.add_item(tr("OPTION_LOW"))
	$Buttons/Reflections/Dropdown.add_item(tr("OPTION_GOOD"))
	$Buttons/Reflections/Dropdown.add_item(tr("OPTION_BETTER"))
	$Buttons/Reflections/Dropdown.selected = Config.reflections
	
	$Buttons/Physics/Dropdown.add_item(tr("OPTION_LOW"))		#30fps
	$Buttons/Physics/Dropdown.add_item(tr("OPTION_GOOD"))		#60fps
	$Buttons/Physics/Dropdown.add_item(tr("OPTION_BETTER"))		#120fps
	$Buttons/Physics/Dropdown.selected = Config.physics
	
	$Buttons/MasterVol/HSlider.value = Config.master_vol
	$Buttons/EffectsVol/HSlider.value = Config.effects_vol
	$Buttons/MusicVol/HSlider.value = Config.music_vol
	$Buttons/DialogVol/HSlider.value = Config.dialog_vol
	
	$Buttons/MouseSpeedX/HSlider.value = Config.mouse_speed.x
	$Buttons/MouseSpeedY/HSlider.value = Config.mouse_speed.y
	$Buttons/MouseSmooth/HSlider.value = Config.mouse_smooth
	$Buttons/FlipY/CheckBox.pressed = Config.mouse_flip_y

func translate_labels():
	$Back.text = tr("BACK")
	
	$Buttons/Language/Label.text = tr("LANGUAGE")
	
	$Buttons/Fullscreen/Label.text = tr("FULLSCREEN")
	$Buttons/RenderScale/Label.text = tr("RENDERSCALE")
	
	$Buttons/AnisoFilter/Label.text = tr("ANISOFILTER")
	$Buttons/MSAA/Label.text = tr("MSAA")
	$Buttons/ShadowRes/Label.text = tr("SHADOWRES")
	$Buttons/ShadowQuality/Label.text = tr("SHADOWQUALITY")
	$Buttons/Reflections/Label.text = tr("REFLECTIONS")
	$Buttons/Physics/Label.text = tr("PHYSICS")
	
	$Buttons/MasterVol/Label.text = tr("MASTERVOL")
	$Buttons/EffectsVol/Label.text = tr("EFFECTSVOL")
	$Buttons/MusicVol/Label.text = tr("MUSICVOL")
	
	$Buttons/MouseSpeedX/Label.text = tr("MOUSESPEEDX")
	$Buttons/MouseSpeedY/Label.text = tr("MOUSESPEEDY")
	$Buttons/MouseSmooth/Label.text = tr("MOUSESMOOTH")
	$Buttons/FlipY/Label.text = tr("MOUSEFLIPY")

func _on_lang_changed(id: int):
	Config.locale = id
	get_tree().reload_current_scene()

func _on_fullscreen_changed(val: bool):
	Config.fullscreen = val

func _on_render_scale_changed(value: float) -> void:
	Config.render_scale = value

func _on_aniso_changed(id: int):
	Config.aniso = id

func _on_msaa_changed(id: int):
	Config.msaa = id

func _on_shadowres_changed(id: int):
	Config.shadow_res = id

func _on_shadowqual_changed(id: int):
	Config.shadow_quality = id

func _on_reflections_changed(id: int):
	Config.reflections = id

func _on_physics_changed(id: int):
	Config.physics = id

func _on_mastervol_changed(val: float):
	Config.master_vol = val

func _on_effectsvol_changed(val: float):
	Config.effects_vol = val

func _on_musicvol_changed(val: float):
	Config.music_vol = val

func _on_dialogvol_changed(val: float):
	Config.dialog_vol = val

func _on_mouse_speed_x_changed(val: float):
	var speed = Config.mouse_speed
	speed.x = val
	Config.mouse_speed = speed

func _on_mouse_speed_y_changed(val: float):
	var speed = Config.mouse_speed
	speed.y = val
	Config.mouse_speed = speed

func _on_mouse_smooth_changed(val: float):
	Config.mouse_smooth = val

func _on_flip_y_changed(val: bool):
	Config.mouse_flip_y = val

func _on_back_pressed():
	emit_signal("close_menu")
