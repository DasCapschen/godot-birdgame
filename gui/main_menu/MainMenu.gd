extends Control

var level_select_shown : bool = false

onready var levelselect = $Levelselect_Bg
onready var options = $Menu_Bg/Options

onready var tween = $Tween
export var tween_speed: = 0.5
var tween_trans = Tween.TRANS_LINEAR
var tween_ease = Tween.EASE_IN_OUT

func _ready():
	options.rect_position.y = options.rect_size.y

	$Menu_Bg/Main/Buttons/Play.text = tr("PLAY")
	$Menu_Bg/Main/Buttons/Load.text = tr("LOAD")
	$Menu_Bg/Main/Buttons/Options.text = tr("OPTIONS")
	$Menu_Bg/Main/Buttons/Exit.text = tr("EXIT")

func _hide_main():
	var main = $Menu_Bg/Main
	var start = main.rect_position.x
	var end = -main.rect_size.x
	tween.interpolate_property($Menu_Bg/Main, "rect_position:x",
		start, end, tween_speed, tween_trans, tween_ease)
	tween.start()

func _show_main():
	var main = $Menu_Bg/Main
	var start = main.rect_position.x
	var end = 0
	tween.interpolate_property($Menu_Bg/Main, "rect_position:x",
		start, end, tween_speed, tween_trans, tween_ease)
	tween.start()

func _on_play_pressed():
	var start = levelselect.rect_position.x
	var width = levelselect.rect_size.x
	var end
	
	if level_select_shown:
		end = start - width
		level_select_shown = false
	else:
		end = start + width
		level_select_shown = true
		
	tween.interpolate_property(levelselect, "rect_position:x", 
		start, end, tween_speed, tween_trans, tween_ease)
	tween.start()

func _on_load_pressed():
	print("TODO")

func _on_options_pressed():
	if level_select_shown:
		_on_play_pressed()
	
	_hide_main()
	yield(tween, "tween_all_completed")
	
	var start = options.rect_position.y
	var end = 0
	
	tween.interpolate_property(options, "rect_position:y",
		start, end, tween_speed, tween_trans, tween_ease)
	tween.start()

func _on_close_options() -> void:
	var end = options.rect_size.y
	var start = options.rect_position.y
	
	tween.interpolate_property(options, "rect_position:y",
		start, end, tween_speed, tween_trans, tween_ease)
	tween.start()
	
	yield(tween, "tween_all_completed")
	
	_show_main()


func _on_exit_pressed():
	get_tree().quit()
