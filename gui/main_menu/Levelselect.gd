extends Panel

#array of array of [ MapName, icon_path, scene_path ]
var maps = []

func _ready():
	$VBoxContainer/Play.text = tr("PLAY")
	$VBoxContainer/Play.connect("pressed", self, "on_play_pressed")
	
	$VBoxContainer/ItemList.clear()
	
	var dir = Directory.new()
	if dir.open("res://maps") == OK:
		dir.list_dir_begin()
		var filename = dir.get_next()
		while filename != "":
			if filename.get_extension() == "tscn" and not dir.current_is_dir():
				var icon = load("res://maps/icon_"+filename.get_basename()+ ".png") as Texture
				if not icon:
					icon = load("res://icon.png") as Texture
				$VBoxContainer/ItemList.add_item( filename.get_basename(), icon )
				maps.append( "res://maps/"+filename )
			filename = dir.get_next()
	pass


func on_play_pressed():
	var scene = maps[ $VBoxContainer/ItemList.get_selected_items()[0] ]
	Events.emit_signal("start_game", scene)
