extends Control

var options_open: = false

func _on_continue_pressed():
	Events.emit_signal("unpause_game")

func _on_options_pressed():
	if options_open:
		close_options()
	else:
		open_options()
	pass

func _on_main_menu_pressed():
	Events.emit_signal("stop_game")

func _on_exit_pressed():
	Events.emit_signal("exit")


func _on_options_close_menu() -> void:
	close_options()

func open_options():
	var start_top = $AlignOptions/Options.anchor_top
	var start_bottom = $AlignOptions/Options.anchor_bottom
	var end_top = 0
	var end_bottom = 1
	var time = 0.5
	var trans_mode = Tween.TRANS_LINEAR
	var ease_mode = Tween.EASE_IN_OUT
	
	$AlignOptions/Tween.interpolate_property(
		$AlignOptions/Options, "anchor_top",
		start_top, end_top, time, trans_mode, ease_mode)
	$AlignOptions/Tween.start()
	$AlignOptions/Tween.interpolate_property(
		$AlignOptions/Options, "anchor_bottom",
		start_bottom, end_bottom, time, trans_mode, ease_mode)
	$AlignOptions/Tween.start()
	options_open = true

func close_options():
	var start_top = $AlignOptions/Options.anchor_top
	var start_bottom = $AlignOptions/Options.anchor_bottom
	var end_top = 1
	var end_bottom = 2
	var time = 0.5
	var trans_mode = Tween.TRANS_LINEAR
	var ease_mode = Tween.EASE_IN_OUT
	
	$AlignOptions/Tween.interpolate_property(
		$AlignOptions/Options, "anchor_top",
		start_top, end_top, time, trans_mode, ease_mode)
	$AlignOptions/Tween.start()
	$AlignOptions/Tween.interpolate_property(
		$AlignOptions/Options, "anchor_bottom",
		start_bottom, end_bottom, time, trans_mode, ease_mode)
	$AlignOptions/Tween.start()
	options_open = false
